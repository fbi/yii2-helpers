<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 2014/12/3
 * Time: 16:10
 */

namespace fbi\helpers;


class Curl {
	protected static $defaultHeaders=[
		'User-Agent'=>'Mozilla/5.0 (Linux; Android 4.2.1; en-us; OPPO FIND7 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19',
		'Accept'=>'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		//'Accept-Encoding'=>'gzip,deflate,sdch',
		'Accept-Language'=>'zh-CN,zh;q=0.8,en;q=0.6',
		'Cache-Control'=>'max-age=0',
		'Connection'=>'close',
	];

	public static function get($url,$gets=[],$headers=[],$withHeader=false){
		$options=[CURLOPT_HEADER=>(bool)($withHeader)];
		if($gets){
			$url.=( strpos($url,'?')?'&':'?' ).http_build_query($gets);
		}
		$options[CURLOPT_HTTPHEADER]=self::buildHeader($headers+['HOST'=>parse_url($url,PHP_URL_HOST)]);
		return self::forward($url,$options);
	}

	public static function post($url,$posts=[],$headers=[],$withHeader=false){
		$options=[CURLOPT_POST=>1,CURLOPT_HEADER=>(bool)($withHeader)];
		if($posts){
			$options[CURLOPT_POSTFIELDS]=http_build_query($posts);
		}
		$options[CURLOPT_HTTPHEADER]=self::buildHeader($headers+['HOST'=>parse_url($url,PHP_URL_HOST)]);
		return self::forward($url,$options);
	}

	public static function head($url,$gets=[],$headers=[]){
		$options=[CURLOPT_NOBODY=>1,CURLOPT_HEADER=>1];
		if($gets){
			$url.=( strpos($url,'?')?'&':'?' ).http_build_query($gets);
		}
		$options[CURLOPT_HTTPHEADER]=self::buildHeader($headers+['HOST'=>parse_url($url,PHP_URL_HOST)]);
		return self::forward($url,$options);
	}

	private static function forward($url,$options=[]){
		$ch = curl_init($url);

		if(strtolower(substr($url,0,8))=='https://'){
			curl_setopt_array($ch,[CURLOPT_SSL_VERIFYHOST=>2,CURLOPT_SSL_VERIFYPEER=>false]);
		}

		$options+=[
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 10,
		];
		curl_setopt_array($ch,$options);
		return curl_exec($ch);
	}

	private static function buildHeader($headers=[]){
		$headerArray=[];
		$headers+=self::$defaultHeaders;
		foreach($headers as $k=>$v){
			if(is_array($v)){
				foreach($v as $vv){
					$headerArray[]=$k.':'.$v;
				}
			}elseif(is_scalar($v)){
				$headerArray[]=$k.':'.$v;
			}
		}
		return $headerArray;
	}

	public static function buildCookie($cookies=[]){
		$cookiesArray=[];
		foreach($cookies as $k=>$v){
			if(is_scalar($v)){
				$cookiesArray[]=$k.'='.urlencode($v);
			}
		}
		return implode(';',$cookiesArray);
	}

	public static function parseHeader($header){
		$cookies=$headers=[];
		if($header){
			foreach( explode("\r\n",$header) as $h ){
				$m = strpos($h,':');
				$k=substr($h,0,$m);
				$v=substr($h,$m+1);
				$k=strtolower(trim($k));
				$v=trim($v);
				if(!$k){continue;}
				if(isset($headers[$k])){//多个一样的header,比如Set-Cookie
					if(is_array($headers[$k])){
						$headers[$k][]=$v;
					}else{
						$headers[$k]=[$headers[$k]];
						$headers[$k][]=$v;
					}
				}else{
					$headers[$k]=$v;
				}
			}
			if(isset($headers['set-cookie'])){
				$th=$headers['set-cookie'];
				if(is_scalar($th)){
					$th=[$th];
				}
				foreach($th as $c){
					$kv = substr($c,0,strpos($c,"; "));
					$eq = strpos($kv,'=');
					$k=substr($kv,0,$eq);
					$v=substr($kv,$eq+1);
					$cookies[$k]=$v;
				}
			}
		}
		return ['header'=>$headers,'cookie'=>$cookies];
	}
}
